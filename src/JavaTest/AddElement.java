package JavaTest;

import java.util.ArrayList;
import java.util.Arrays;

public class AddElement {
	public static void main(String[] args) {
		
		Integer array[] = {2,9,5,0};  
		
		System.out.println("Array:"+Arrays.toString(array));  
		
		ArrayList<Integer> arrayList = new ArrayList<Integer>(Arrays.asList(array));  
		arrayList.add(4);  
		array = arrayList.toArray(array); 
		
		System.out.println("Array after adding element: "+Arrays.toString(array));   
	}

	//
}
